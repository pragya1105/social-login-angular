import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';

import {
  
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  submitted:boolean = false;
  constructor(
    private fb: FormBuilder, 
    private utils: UtilsService,
    private api: HttpService,
    private router: Router,
    private socialAuthService : AuthService
  ) {
      

  }

  ngOnInit() {
    this.initForm()
  }

  initForm(){
    this.loginForm = this.fb.group({
      email: [  "", [Validators.required, Validators.email]],
      name: [ "", Validators.required],
      mobileNumber: [ "", Validators.required]
    });
  }

  get f(){ return this.loginForm.controls; }

  login(){

    this.api.isSocailUserExist({
      socail_id : this.socailData.id
    }).subscribe(
      data=> {
        console.log(data)
        if(data['response'] && data['response'].is_signup_complete == '1'){
          this.utils.setCookie('token', data['response'].access_token);
          this.utils.alert('success', 'logged in successfully');
          this.router.navigate(['/profile']);
        } else {
          this.is_show = true
          this.loginForm = this.fb.group({
            email: [ this.socailData.email || '' , [Validators.required, Validators.email]],
            name: [ this.socailData.name || '' , Validators.required],
            mobileNumber: [ this.socailData.mobile || '', Validators.required]
          });

        }
      },
      error=> {
        debugger
        this.utils.alert('error', error['error']['message']);
      }
    );
  }




  socailData
  is_show = false
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } 
   
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        this.socailData = userData

        if(this.socailData)
        this.login()
      }
    );
  }


  Signup(){

    this.submitted = true;
       if(this.loginForm.invalid) return;


       this.loginForm.value.socail_id = this.socailData.id
       this.loginForm.value.socail_type  = this.socailData.provider
       this.loginForm.value.profileImage = this.socailData.image

       this.api.CompleteSignup(this.loginForm.value).subscribe(
        data=> {
          if(data['response']){
              this.utils.setCookie('token', data['response'].access_token);
              this.utils.alert('success', 'logged in successfully');
              this.router.navigate(['/profile']);
          }
        },
        error=> {
          debugger
          this.utils.alert('error', error['error']['message']);
        }
      );

        
}


}
