import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilsService } from './utils.service';
@Injectable({
    providedIn: 'root'
})
export class AuthDataService {

    constructor(private router: Router , private cookie : UtilsService) { }

    isLoggedIn() {
        var token =this.cookie.getCookie('token');
        if (token) return true;
        else return false;
    }
}

