import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

 
  constructor(
    private toast: ToastrService
  ) { }

  setCookie(key, data){
    document.cookie = key + "=" + data
  }

  getCookie(key){
    let allcookies = document.cookie;
    let cookiearray = allcookies.split(';');
    
    let data = cookiearray.find((cookie)=>cookie.split('=')[0].trim() == key)
    
    if(data && data.split('=')[1] != '')
        return data.split('=')[1]
    else
      return ''    

  }

  /*toaster*/
  alert(type, msg){
    switch (type) {
      case "success":
        this.toast.success(msg, 'SUCCESS');
        break;
      case "info":
        this.toast.info(msg, 'INFORMATION');
        break;
      case "error":
        this.toast.error(msg, 'ERROR');
        break;
      case "warn":
        this.toast.warning(msg, 'WARNING');
        break;
      default:
        this.toast.success(msg, 'SUCCESS');
        break;
    }
  }

 
}
