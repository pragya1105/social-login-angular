import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { UtilsService } from './utils.service';
@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
    constructor(private cookie : UtilsService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = this.cookie.getCookie('token');

        let url = environment.baseUrl;
        
        url += req.url;

        const copiedReq = req.clone({
            headers: req.headers.append('access_token', token), url: url
        });
        return next.handle(copiedReq);
    }
}